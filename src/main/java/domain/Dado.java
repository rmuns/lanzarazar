/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package domain;

import lombok.Data;

/**DADO
 * "Dado" porque puede representar cualquier objeto que se lanze y de un resultado
 * como una moneda de dos caras
 * @author Ricard
 */
@Data
public class Dado implements Comparable<Dado>{
    private int id;
    private int caras = 2; //moneda
    private int caraSuperior = 1;
    
    /**
     * Comparamos a partir de caras para ordenar de menor a mayor
     * El id no cambiara para no perder de vista el orden.
     * @param dado
     * @return 
     */
    @Override
    public int compareTo(Dado dado) {
        return Integer.compare(this.caras, dado.caras);
    }
    
    /**
     * @return the caras
     */
    public int getCaras() {
        return caras;
    }

    /**
     * @param caras the caras to set
     */
    public void setCaras(int caras) {
        this.caras = caras;
    }

    /**
     * @return the caraSuperior
     */
    public int getCaraSuperior() {
        return caraSuperior;
    }

    /**
     * @param caraSuperior the caraSuperior to set
     */
    public void setCaraSuperior(int caraSuperior) {
        this.caraSuperior = caraSuperior;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.lanzarAzar.boot.services;

import domain.Dado;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import org.springframework.stereotype.Service;

/**DadosService
 *
 * @author Ricard
 */
@Service
public class DadosService {
    List<Dado> cubilete = new ArrayList<>();
    private int ultimoId = 0;
    
    /**llenarCubilete
     * Rellena la lista con los objeto Dado
     * @param numeroCaras
     * @param numeroDados 
     */
    public void llenarCubilete(int numeroCaras, int numeroDados){
        for (int i = 1 ; i <= numeroDados ; i++) {
            setUltimoId( getUltimoId() + 1);
            Dado dado = new Dado();
            dado.setId(getUltimoId());
            dado.setCaras(numeroCaras);
            lanzarDado(dado); //Obtener valor inicial
            this.cubilete.add(dado);
        }
    }
    
    /**lanzaCubilete
     * Volvemos a randomizar el contenido de la lista
     */
    public void lanzaCubilete(){
        for (Dado dado : this.cubilete) {
            lanzarDado(dado);
        }
    }
    
    /**lanzarDado
     * Metodo aleatorio para elegir la cara boca arriba
     * @param dado Enviamos un objeto dado para dar valor a caraSuperior
     */
    private void lanzarDado(Dado dado){
        int valorMinimoCaras = 1;
        int valorMaximoCaras = dado.getCaras();
        Random random = new Random();
        int caraSuperior = random.nextInt(valorMaximoCaras - valorMinimoCaras + 1) + valorMinimoCaras;
        dado.setCaraSuperior(caraSuperior);
    }
    
    /**vaciaCubilete
     * Eliminar lista y regresar a cero indicador de id
     */
    public void vaciaCubilete(){
        cubilete.clear();
        setUltimoId(0);
    }
    
    /**getCubilete
     * Ordena por numero de caras
     * @return Devuelve lista vacia
     */
    public List<Dado> getCubilete(){
        Collections.sort(this.cubilete);
        return this.cubilete;
    }
    
    /**getUltimoId
     * @return the ultimoId
     */
    public int getUltimoId() {
        return ultimoId;
    }
    
    /**setUltimoId
     * @param ultimoId the ultimoId to set
     */
    public void setUltimoId(int ultimoId) {
        this.ultimoId = ultimoId;
    }
}

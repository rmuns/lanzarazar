/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.lanzarAzar.boot.controller;

import com.lanzarAzar.boot.services.DadosService;
import domain.Dado;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *http://localhost:8080/llenarCubilete
 * @author Ricard
 */
@Controller
public class ControllerDados {
    @Autowired
    private DadosService DadosService;
    
    @GetMapping("/llenarCubilete")
    public String formularioDado(
            @RequestParam(name="bolsa", required=false, defaultValue="no") String bolsa,
            Model model
    ){
        List<String> options = new ArrayList<>();
        options.add("2");
        options.add("4");
        options.add("6");
        options.add("8");
        options.add("10");
        options.add("12");
        options.add("20");
        model.addAttribute("options", options);
        model.addAttribute("valorDefectoCara", 6);
        model.addAttribute("valorDefectoNumero", 1);
        if(!bolsa.equals("no")){
            DadosService.vaciaCubilete();
        }
        return "formulario";
    }
    
    @PostMapping("/llenarCubilete")
    public String lanzarDados(
            @RequestParam(name = "caras") int caras,
            @RequestParam(name = "numeroDeDados") int numeroDeDados,
            Model model
    ){
        DadosService.llenarCubilete(caras, numeroDeDados);
        List<Dado> listaDados = DadosService.getCubilete();
        model.addAttribute("dados", listaDados);
        return "mesa";
    }
    
    @GetMapping("/mesa")
    public String verMesa(
            @RequestParam(name="lanzar", required=false, defaultValue="no") String bolsa,
            Model model
    ){
        List<Dado> listaDados = DadosService.getCubilete();
        model.addAttribute("dados", listaDados);
        if(!bolsa.equals("no")){
            DadosService.lanzaCubilete();
        }
        return "mesa";
    }
}

package com.lanzarAzar.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * http://localhost:8080/llenarCubilete
 * @author Ricard
 */
@SpringBootApplication
public class AzarApplication {
	public static void main(String[] args) {
		SpringApplication.run(AzarApplication.class, args);
	}
}
